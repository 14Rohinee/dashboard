@extends('layouts.dashboard')

@section('title')
    Add User
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection


@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Create User Form</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Home</a>
                </li>
                <li>
                    <a href="{{ route('users.index') }}">Users</a>
                </li>
                <li class="active">
                    <strong>Add User</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('users.index') }}" class="btn btn-warning mt-3" id="save-data">Cancle</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('users.show',$show->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Personal Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group @error('name') has-error @enderror">
                                            <label for="name">Name <span class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter name" class="form-control" name="name" id="name" value="{{ $show->name }}">
                                            @error('name')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="male" value="male" name="gender" checked="" value="male" {{ $show->gender == 'other' ? 'checked' : '' }}>
                                                <label for="male"> Male </label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="female" value="female" name="gender" value="female" {{ $show->gender == 'other' ? 'checked' : '' }}>
                                                <label for="female"> Female </label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" id="other" value="other" name="gender" value="other" {{ $show->gender == 'other' ? 'checked' : '' }}>
                                                <label for="other"> Other </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group @error('dob') has-error @enderror">
                                            <label for="dob">Date Of Birth <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control datepicker" placeholder="Enter DOB" name="dob" id="dob" autocomplete="off" value="{{ $show->dob }}">
                                            @error('dob')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group @error('email') has-error @enderror">
                                            <label for="email">Email <span class="text-danger">*</span></label>
                                            <input type="email" class="form-control" placeholder="Enter email" name="email" id="email" value="{{ $show->email }}">
                                            @error('email')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group @error('password') has-error @enderror">
                                            <label for="password">Password <span class="text-danger">*</span></label>
                                            <input type="password" class="form-control" placeholder="Enter password" name="password" id="password" value="{{ $show->password }}">
                                            @error('password')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group @error('contact_no') has-error @enderror">
                                            <label for="contact_no">Contact <span class="text-danger">*</span></label>
                                            <input type="number" class="form-control" placeholder="Enter Contact number" name="contact_no" id="contact_no" value="{{ $show->contact_no }}">
                                            @error('contact_no')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Family Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group @error('father_name') has-error @enderror">
                                        <label for="father_name">Father Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter father name" class="form-control" name="father_name" id="father_name" value="{{ $show->father_name }}">
                                        @error('father_name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('mother_name') has-error @enderror">
                                        <label for="mother_name">Mother Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter mother name" name="mother_name" id="mother_name" value="{{ $show->mother_name }}">
                                        @error('mother_name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('marital_status') has-error @enderror">
                                        <label for="marital_status">Marital Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="marital_status" id="marital_status">
                                            <option>Select</option>
                                            <option value="other" {{ $show->marital_status == 'married' ? 'selected' : '' }}>Married</option>
                                            <option value="other" {{ $show->marital_status == 'unmarried' ? 'selected' : '' }}>Unmarried</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('spouse_name') has-error @enderror">
                                        <label for="spouse_name">Spouse Name</label>
                                        <input type="text" class="form-control" placeholder="Enter spouse name" name="spouse_name" id="spouse_name" value="{{ $show->spouse_name }}">
                                        @error('spouse_name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('anniversary') has-error @enderror">
                                        <label for="anniversary">Anniversary Date</label>
                                        <input type="text" class="form-control datepicker" placeholder="Enter anniversary date" name="anniversary" id="anniversary" autocomplete="off" value="{{ $show->anniversary }}">
                                        @error('anniversary')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Bank Detail</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group @error('bank_name') has-error @enderror">
                                        <label for="bank_name">Bank Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter bank name" class="form-control" name="bank_name" id="bank_name" value="{{ $show->bank_name }}">
                                        @error('bank_name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('bank_branch') has-error @enderror">
                                        <label for="bank_branch">Bank Branch <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter bank branch name" name="bank_branch" id="bank_branch" value="{{ $show->bank_branch }}">
                                        @error('bank_branch')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('account_number') has-error @enderror">
                                        <label for="account_number">Account Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter account number" name="account_number" id="account_number" value="{{ $show->account_number }}">
                                        @error('account_number')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('ifsc_code') has-error @enderror">
                                        <label for="ifsc_code">IFSC Code <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter ifsc code" name="ifsc_code" id="ifsc_code" value="{{ $show->ifsc_code }}">
                                        @error('ifsc_code')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Uploaded Photo</h5>
                        </div>
                        <div class="ibox-content">
                            <input type="file" class="dropify" name="file" data-default-file="{{ Auth::user()->profile_photo }}" />
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $('.dropify').dropify();

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $(document).on('click', '#save-data', function(){
            $('form').submit();
        });


    </script>

@endsection

