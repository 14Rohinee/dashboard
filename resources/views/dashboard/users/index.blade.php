@extends('layouts.dashboard')

@section('title')
    Users
@endsection

@section('css')
    <link href="{{ asset('asset/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
        <h2>Users</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">Home</a>
            </li>
            <li class="active">
                <strong>Users @lang('smile.happy')</strong>
            </li>
        </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('users.create') }}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add User</a>
        </div>
    </div>
@endsection

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Users List
                    </h5>
                </div>
                <div class="ibox-content">

                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover user-table" >
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>DOB</th>
                            <th>Email</th>                            <th>Contact No</th>
                            <th>Status</th>
                            <th>Father_Name</th>
                            <th>Mother_Name</th>
                            <th>Marital_Status</th>
                            <th>Spouse_Name</th>
                            <th>Anniversary</th>
                            <th>Bank_Name</th>
                            <th>Bank_Branch</th>
                            <th>Account_Number</th>
                            <th>IFSC_Code</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>
                                    <img src="{{ $user->profile_photo }}" alt="" height="60" width="60">
                                </td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->gender }}</td>
                                <td>{{ $user->dob }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->contact_no }}</td>
                                <td>
                                    @if ($user->status == 'active')
                                        <span class="label label-primary">Active</span>
                                    @else
                                        <span class="label label-danger">In-Active</span>
                                    @endif
                                </td>
                                <td>{{ $user->father_name}}</td>
                                <td>{{ $user->mother_name }}</td>
                                <td>

                                    @if ($user->marital_status == 'married')
                                    <span class="label label-primary">Married</span>
                                @else
                                    <span class="label label-danger">Un-Married</span>
                                @endif
                                </td>
                                <td>{{ $user->spouse_name }}</td>
                                <td>{{ $user->anniversary }}</td>
                                <td>{{ $user->bank_name }}</td>
                                <td>{{ $user->bank_branch }}</td>
                                <td>{{ $user->account_number }}</td>
                                <td>{{ $user->ifsc_code }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{route('users.show',$user->id) }}" class="btn-white btn btn-xs" >View</a>

                                        <a href="{{route('users.edit',$user->id) }}" class="btn-white btn btn-xs">edit</a>

                                        <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn-white btn btn-xs">Delete</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>
        </div>
    </div>






    {{-- @include('users.modal.create') --}}
@endsection

@section('script')
    {{-- Datatable --}}
    <script src=" {{ asset('asset/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/sweetalert/sweetalert.min.js') }}"></script>



    <!-- Page-Level Scripts -->


@endsection
