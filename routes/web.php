<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();


Route::get('/dashboard', function () {
    return view('dashboard.dashboard.index');
})->name('dashboard');

// User CRUD
Route::resource('users', UserController::class);
