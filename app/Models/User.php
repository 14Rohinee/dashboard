<?php

namespace App\Models;

use App\Observers\UserObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;


    protected static function boot() {
        parent::boot();
        static::observe(UserObserver::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Mutator
    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = date('Y-m-d', strtotime($value));
    }

    // Accessor
    public function getDobAttribute($value)
    {
        return date('d M, Y', strtotime($value));
    }

    public function getProfilePhotoAttribute()
    {
        $defaultPath = 'https://www.kindpng.com/picc/m/65-653274_workers-compensation-law-social-security-disability-user-icon.png';

        $path = asset('user-uploads/users/'.$this->photo);

        return $this->photo == '' ? $defaultPath : $path;
    }

}
